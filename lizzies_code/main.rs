fn main() {
    let mut draft_board: [[Square; 8]; 8] = [[None; 8]; 8];
    let specials = [Piece::Rook, Piece::Knight, Piece::Bishop, Piece::King, Piece::Queen, Piece::Bishop, Piece::Knight, Piece::Rook];
    let pawns = [Piece::Pawn; 8];
    draft_board[0] = specials.map(|p| Some((Color::White, p)));
    draft_board[1] = pawns.map(|p| Some((Color::White, p)));
    draft_board[6] = pawns.map(|p| Some((Color::Black, p)));
    draft_board[7] = specials.map(|p| Some((Color::Black, p)));
    let board = Board::new(draft_board);
    println!("{:?}", board.get(2, 5));
}

#[derive(Debug, Clone, Copy)]
enum Piece {
    Pawn,
    Knight,
    Bishop,
    Rook,
    Queen,
    King,
}

#[derive(Debug, Clone, Copy)]
enum Color {
    White,
    Black,
}

type Square = Option<(Color, Piece)>;

#[derive(Debug, Clone, Copy)]
struct Board([Row; 8]);
impl Board {
    fn new(friendly: [[Square; 8]; 8]) -> Board {
        let half = friendly.map(|row| {
            row.map(|cell| 
                match cell {
                    None => 0b1111u32,
                    Some((color, piece)) => {
                        let color_bit = match color {
                            Color::Black => 8,
                            Color::White => 0
                        };
                        let piece_bits = match piece {
                            Piece::Pawn => 0,
                            Piece::Knight => 1,
                            Piece::Bishop => 2,
                            Piece::Rook => 3,
                            Piece::Queen => 4,
                            Piece::King => 5,
                        };
                        color_bit + piece_bits
                    }
                }
            )
        });
        Board({
            half.map(|row| {
                let mut row_out = 0u32;
                for n in row {
                    row_out <<= 4;
                    row_out |= n;
                }
                Row(row_out)
            })
        })
    }
    fn get_row(&self, idx: usize) -> Row {
        self.0[idx]
    }
    fn get(&self, column: usize, row: usize) -> Square {
        self.get_row(row).get_square(column)
    }
}

#[derive(Debug, Clone, Copy)]
struct Row(u32);
impl Row {
    fn get_square(self, idx: usize) -> Square {
        let mask = 0b1111u32;
        let shift = (7 - idx) * 4;
        let square = (self.0 >> shift) & mask;
        let (piecenum, color) = match square >= 8 {
            true => (square - 8, Color::Black),
            false => (square, Color::White)
        };
        let piece = match piecenum {
            0 => Piece::Pawn,
            1 => Piece::Knight,
            2 => Piece::Bishop,
            3 => Piece::Rook,
            4 => Piece::Queen,
            5 => Piece::King,
            _ => return None
        };
        Some((color, piece))
    }
}