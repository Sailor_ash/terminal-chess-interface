use regex::Regex;

#[derive(Debug, PartialEq, Eq)]
enum PlayerType {
    Local,
    CPU,
    Online,
}

#[derive(Debug)]
enum Piece {
    Pawn,
    Knight,
    Bishop,
    Rook,
    Queen,
    King,
}

#[derive(Debug)]
enum Color {
    White,
    Black,
}

pub struct Game {
    bitmap: [u32; 8],
    gamelog: Vec<String>,
    white: Player,
    black: Player,
    whites_turn: bool,
    outcome: Option<[f32; 2]>,
}

impl Game {
    fn new(white: Player, black: Player) -> Game {
        Game {
            bitmap: setup_board(),
            gamelog: vec![],
            white,
            black,
            whites_turn: true,
            outcome: None,
        }
    }
    fn draw_board(&self) {
        for i in 0..8 {
            for j in 0..8 {
                print!("{:?} ", &self.read_square([i, j]));
            }
            println!();
        }
    }

    fn read_square(&self, position: [usize; 2]) -> Option<(Color, Piece)> {
        let col = &self.bitmap[position[1]];
        let mask = 15;
        let mut square = (col >> 4 * position[0]) & mask;
        if square == 0 {
            return None;
        };
        let color;
        if square > 8 {
            square -= 8;
            color = Color::Black;
        } else {
            color = Color::White;
        }
        let piece = match square {
            1 => Piece::Pawn,
            2 => Piece::Knight,
            3 => Piece::Bishop,
            4 => Piece::Rook,
            5 => Piece::Queen,
            6 => Piece::King,
            _ => panic!(),
        };
        Some((color, piece))
    }

    fn get_input(&self) -> String {
        if self.whites_turn {
            if self.white.playertype == PlayerType::Local {
                get_local_input(&self, "white".into())
            } else if self.white.playertype == PlayerType::Online {
                todo!();
            } else {
                todo!();
            }
        } else {
            if self.black.playertype == PlayerType::Local {
                get_local_input(&self, "black".into())
            } else if self.black.playertype == PlayerType::Online {
                todo!();
            } else {
                todo!();
            }
        }
    }

    fn validate_move(&self, player_move: String) -> bool {
        let r0 = Regex::new("([a-h][1-8]){2}[q,r,b,n]?").unwrap();
        let r1 = Regex::new(
            "^((([a-h,N,B,R,Q,K][a-h]?[1-8]?x)?[a-h][1-8][+,#,=]?)|0-0-0|0-0|O-O-O|O-O)$",
        )
        .unwrap();
        if r0.is_match(&player_move) {
            let from = pos_to_tuple(&player_move[..2]nto()));
            let to = pos_to_tuple(&player_move[2..].into());
            let piece = &self.read_square(from);
            return check_legal_move(&self, piece, from, to);
        } else if r1.is_match(&player_move) {
            todo!();
        }
        println!("{} is not a legal move. Please enter a move in UCI format. Ex: e2e4 or e1g1", player_move);
        false
    }
}

pub struct Player {
    name: String,
    rating: usize,
    nation: String,
    playertype: PlayerType,
    ip: String,
}

impl Player {
    fn new(
        name: String,
        rating: usize,
        nation: String,
        playertype: PlayerType,
        ip: String,
    ) -> Player {
        Player {
            name,
            rating,
            nation,
            playertype,
            ip,
        }
    }
}

fn main() {
    let white = Player::new(
        "Avery Cate".into(),
        3000,
        "United States".into(),
        PlayerType::Local,
        "".into(),
    );
    let black = Player::new(
        "Magnus Carlson".into(),
        2849,
        "Norway".into(),
        PlayerType::Local,
        "".into(),
    );
    let game = Game::new(white, black);
    while game.outcome.is_none() {
        game.draw_board();
        let player_move = loop {
            let player_move = game.get_input();
            if game.validate_move(player_move) {
                break player_move;
            }
        };
        //game.move(player_move);
        //game.check_gamestate();
    }
}

fn setup_board() -> [u32; 8] {
    [
        0b01000001000000000000000010011100,
        0b00100001000000000000000010011010,
        0b00110001000000000000000010011011,
        0b01010001000000000000000010011101,
        0b01100001000000000000000010011110,
        0b00110001000000000000000010011011,
        0b00100001000000000000000010011010,
        0b01000001000000000000000010011100,
    ]
}

fn get_local_input(game: &Game, color: String) -> String {
    println!("{} to move::", color);
    let mut s = String::new();
    std::io::stdin().read_line(&mut s).unwrap();
    s
}

fn pos_to_tuple(pos: String) -> [usize;2] {
    let mut tuple = [usize;2];
    tuple[0] = pos.0 as usize -97;
    tuple[1] = pos.1. as usize -1;
    tuple
}

fn check_legal_move(game: Game, piece: (Color, Piece), from: String, to: String) -> bool {
    false
    //todo!();
}
